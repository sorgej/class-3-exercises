// JSCRIPT 200 Class 2 Homework 3

// Class Exercise 1. Class 1 Exercise 4 Page 34
//^(\w+)@(\w+)\.(\w+)$


// Class Exercise 2. Class 3 Exercise 1 Page 8
// Battle Game
function attack(attackingPlayer, defendingPlayer, baseDamage, variableDamage) {
  let damageTotal = baseDamage + Math.random()*variableDamage;
  damageTotal = damageTotal.toFixed(0);
  defendingPlayer.health -= damageTotal;
  return console.log(`${attackingPlayer.name} does ${damageTotal} damage to ${defendingPlayer.name}'s base!`);
};

var player1 =  {};
player1 = {
  health: 10,
  name: 'Blackbeard',
};

var player2 =  {};
player2 = {
  health: 10,
  name: 'Long John Silver',
};

// test to see function and player2 health
attack(player1, player2,5,5);
console.log(player2.health);


// Class 3 Exercise 2 Page 17
// Itemized Receipt +1 Extra Credit

// for multiple objects, objects.forEach(logReceipt);
const Product = function Product(name, price) {
  this.descr = name;
  this.price = price;
  this.logPrice = function(name, price) {
    console.log(`${this.descr} - $${this.price}`);
  };
};

function logReceipt () {
  var priceTotal = 0;
  // console.log(priceTotal);
  const objects = Array.from(arguments);
  const taxRate = objects[0];
  objects.splice(0,1);
  //console.log(arguments);
  console.log(objects);

  objects.forEach(function (item){
    item.logPrice();
    priceTotal += item.price;
    //console.log(`Total price is now $${priceTotal}`);
    return(priceTotal);
  }) ;
  //{ priceTotal += this.price});
  console.log(`Subtotal - $${priceTotal}`);
  console.log(`Tax - $${taxRate}`);
  priceTotal = priceTotal + priceTotal*taxRate;
  console.log(`Total - $${priceTotal}`);
  return priceTotal;
};

const object1 = new Product('Video Game', 100.00);
//console.log(object1);

const object2 = new Product('Light Saber', 3000.00);
//console.log(object2);

//test
logReceipt(0.1, object1, object2);


// Class 3 Exercise 3 Page 27
// new Spaceship

const SpaceShip= function SpaceShip(name, topSpeed) {
  const shipName = name;
  let shipSpeed = topSpeed;
  this.accelerate = function() {
    console.log(`${shipName} moving to ${shipSpeed} mph!`);
  };
  this.changeSpeed = function(newSpeed) {
    shipSpeed = newSpeed;
  };
};

const BattleCruiser = new SpaceShip('Battle Cruiser', 50000);
const Peewee = new SpaceShip('Peewee', 3000);
const Tanker = new SpaceShip('Tanker', 10000);

Peewee.accelerate();
Tanker.accelerate();
BattleCruiser.accelerate();
console.log(`${BattleCruiser.shipName} top speed is: ${BattleCruiser.topSpeed}.`);
BattleCruiser.changeSpeed(44,000);
BattleCruiser.accelerate();
Tanker.changeSpeed(8000);
Tanker.accelerate();
console.log('Woohoo!');


// Class 3 Exercise 2 (Online)
// Get the Phone number

// '(206) 333-4444'
// '206-333-4444'
// '206 333 4444'

var testPhoneNumber = function(phoneNumber) {
  const testExp = /\(?\d{3}\)?\s?\-?\d{3}\s?\-?\d{4}/;
  let numberTest = testExp.test(phoneNumber);
  console.log(numberTest);
  return numberTest;
}

let number1 = '(206) 333-4444';
let number2 = '206-333-4444';
let number3 = '206 333 4444';
let number4 = '206 -333 )4444';

testPhoneNumber(number1);
testPhoneNumber(number2);
testPhoneNumber(number3);
testPhoneNumber(number4);

var parsePhoneNumber = function(phoneNumber) {
  const areaTest = /\d{3}/;
  let area_code = areaTest.exec(phoneNumber);
  const numberTest = /\d{3}\s?\-?\d{4}/;
  let number = numberTest.exec(phoneNumber);
  let numberObject = {areaCode: area_code[0], phone_number: number[0].replace( /\D/g, '')};
  console.log(numberObject);
  return numberObject;
}

parsePhoneNumber(number1);
parsePhoneNumber(number2);
parsePhoneNumber(number3);


// Class 3 Exercise 3 (Online)
// Soccer Standings

const RESULT_VALUES = {
  w: 3,
  d: 1,
  l: 0
}

// This function accepts one argument, the result, which should be a string
// Acceptable values are 'w', 'l', or 'd'
const getPointsFromResult = function getPointsFromResult(result) {
  return RESULT_VALUES[result];
}

// Create getTotalPoints function which accepts a string of results
// including wins, draws, and losses i.e. 'wwdlw'
// Returns total number of points won
var getTotalPoints = function(recordString) {
  //let records = recordString.split("");
  let score = 0;
  for (i in recordString) {
    score += getPointsFromResult(recordString[i]);
  };    
  return score;
};

// Check getTotalPoints
console.log(getTotalPoints('wwdl')); // should equal 7

// create orderTeams function that accepts as many team objects as desired, 
// each argument is a team object in the format { name, results }
// i.e. {name: 'Sounders', results: 'wwlwdd'}
// Logs each entry to the console as "Team name: points"
const orderTeams = function orderTeam() {
  const objects = Array.from(arguments);
  objects.forEach(function(item){
    console.log(item.name);
    console.log(getTotalPoints(item.results));
  });
};

// Check orderTeams
orderTeams(
  {name: 'Sounders', results: 'wwdl'},
  {name: 'Galaxy', results: 'wlld'}
); 
// should log the following to the console:
// Sounders: 7
// Galaxy: 4